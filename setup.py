#!/usr/bin/env python

from distutils.core import setup


setup(name='auto_checker',
      version='1.0',
      description='python course auto checker for Technion',
      author='Guy Shilman',
      author_email='shilmanguy@gmail.com',
      url='https://gitlab.com/g_shilman/python-auto-checker-for-technion',
      requires=['colorama',
                'jupyter'
                ],
      packages=['auto_checker']
      )
