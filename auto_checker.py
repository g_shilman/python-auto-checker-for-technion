from os import mkdir, getcwd
from subprocess import run, PIPE
from pathlib import Path
import zipfile
import re
import colorama

PYTHON_SOURCES_DIR = './python_sources/'
TESTS_ZIP_FILE_PATH = './Hw*_Examples.zip'
TESTS_FILES_DIR_PATH = './test_files/'

# make directory for python files
try:
    mkdir(PYTHON_SOURCES_DIR)
except FileExistsError:
    pass

# init the colored prints. CRUCIAL for windows support
colorama.init(autoreset=True)


def get_hw_number():
    try:
        # get the name of the zipfile of the test files
        tests_zip_file = Path(
            list(Path('.').rglob(TESTS_ZIP_FILE_PATH))[0]).name
    except IndexError:
        # file not found
        print(colorama.Fore.YELLOW +
              f'[WARNING]\tzip file of tests not found --- copy it to {getcwd()}')
        return
    # get rid of everything but the number of the HW
    return int(tests_zip_file.split('_')[0].replace('Hw', ''))


def make_python_file(ipynb_file_path, output_directory):
    # run the jupyter's command to make a python file from a notebook
    run([
        'jupyter', 'nbconvert', f'--output-dir={str(output_directory)}',
        '--to', 'python',
        str(ipynb_file_path)
    ])
    # return the relative path of the new python file
    return str(Path(output_directory).joinpath(ipynb_file_path.name)).replace(
        '.ipynb', '.py')


def extract_tests(tests_zip_file_path, test_files_dir_path):
    try:
        # build the zipfile's name
        tests_zip_file = list(Path('.').rglob(tests_zip_file_path))[0]
        # open the test files zip
        tests_zip_file = zipfile.ZipFile(tests_zip_file, mode='r')
        # extract all test files to a directory
        tests_zip_file.extractall(path=test_files_dir_path)
    except:
        print('Error! could not extract test files.')

    print('extracted all python test files')


def run_all_tests(python_file_path, test_files_dir_path):
    did_fail_any_test = False

    # get the question number
    question_number = int(
        re.sub('hw[0-9]+q', '',
               Path(python_file_path).name).replace('.py', ''))
    # test all relevant input files
    for test_file_input in Path(test_files_dir_path).rglob(
            f'*q{question_number}in*'):
        # open the output file
        script_output = PYTHON_SOURCES_DIR + 'real_output_' + \
            Path(str(test_file_input).replace('in', 'out')).name
        with open(script_output, mode='wb+') as output_file:
            # open the input file
            with open(test_file_input, mode='rb') as input_data:
                # run the answer file with the given input and save the output
                run(['python', str(python_file_path)],
                    stdin=input_data,
                    stdout=output_file)
            output_file.seek(0, 0)
            real_output = output_file.read()
            # get the matching output file
            with open(str(test_file_input).replace('in', 'out'),
                      mode='rb') as expected_output:
                # read both files and make both LF style
                if expected_output.read().replace(b'\r\n', b'\n') == real_output.replace(b'\r\n', b'\n'):
                    print(
                        colorama.Fore.GREEN +
                        f'[INFO]\t\ttest {Path(python_file_path).name} with {test_file_input} passed')
                else:
                    print(
                        colorama.Fore.RED +
                        f'[INFO]\t\ttest {Path(python_file_path).name} with {test_file_input} failed!')
                    did_fail_any_test = True

    return not did_fail_any_test


# extract test files to TESTS_FILES_DIR_PATH
extract_tests(TESTS_ZIP_FILE_PATH, TESTS_FILES_DIR_PATH)

hw_number = get_hw_number()
# create the results zip file and add the students.txt file to it
results_zip_file = zipfile.ZipFile('results.zip', mode='w')
try:
    results_zip_file.write('students.txt', 'students.txt')
except FileNotFoundError:
    print(colorama.Fore.YELLOW +
          '[WARNING]\tstudents.txt file not given --- add to base directory to automatically add students.txt file to results zip file')

# go over all .ipynb files
for ipynb_file_path in Path.cwd().glob('*.ipynb'):
    # make python files in the PYTHON_SOURCES_DIR directory
    python_file_path = make_python_file(ipynb_file_path, PYTHON_SOURCES_DIR)
    # test python file
    did_pass = run_all_tests(python_file_path, TESTS_FILES_DIR_PATH)
    if did_pass:
        # add to result zip successful HW answers
        results_zip_file.write(python_file_path, Path(python_file_path).name)
