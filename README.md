1. extract this archive.
2. open cmd in the extracted directory.
3. to install required python libraries run:
	python -m pip install -r requirements.txt
4. run auto_checker.py in the directory with the ipynb files of the HW.
   **don't forget to put in this directory the zipfile of the tests and the your students.txt file.**
5. enjoy ;)
